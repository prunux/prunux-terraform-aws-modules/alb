variable "name" {
  description = "A name for this ALB"
}

variable "env" {
  description = "the environment for and of this ALB"
  default     = "production"
}

variable "application" {
  description = "the application tag for and of this ALB"
}

variable "internal" {
  description = "If true, the LB will be internal. Defaults to false."
  default     = false
}

variable "subnets" {
  description = "List of subnets the LB endpoints will be in"
  type        = "list"
}

variable "ip_address_type" {
  description = "IP address type to use for the ALB. Defaults to 'ipv4' (alternative 'dualstack')"
  default     = "ipv4"
}

variable "vpc_id" {
  description = "VPC the LB endpoints will be in"
}

variable "instances" {
  description = "List of instances to add to the LB"
  type        = "list"
  default     = []
}

variable "target_port_http" {
  description = "HTTP Port of the target(s), defaults to 80"
  default     = 80
}

variable "target_port_https" {
  description = "HTTPS Port of the target(s), defaults to 443"
  default     = 443
}

variable "target_type" {
  description = "The type of the target to register with, defaults to instance"
  default     = "instance"
}

variable "ssl_certificate_id" {
  description = "SSL Certificate arn, if not specified https listener won't be created"
  default     = "none"
}

variable "https_only" {
  description = "if set the HTTP port won't be created"
  default     = "false"
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle"
  default     = 60
}

variable "health_check_healthy_threshold" {
  description = "The number of consecutive successful health checks that must occur before declaring the backend healthy"
  default     = 10
}

variable "health_check_unhealthy_threshold" {
  description = "The number of consecutive failed health checks that must occur before declaring the backend unhealthy"
  default     = 2
}

variable "health_check_timeout" {
  description = "The amount of time to wait when receiving a response from the health check, in seconds"
  default     = 5
}

variable "health_check_path" {
  description = "Path the health check will use"
  default     = "/"
}

variable "health_check_interval" {
  description = "The amount of time between health checks of an individual instance, in seconds"
  default     = 10
}

variable "health_check_matcher" {
  description = "The HTTP codes to use when checking for a successful response from a target"
  default     = "200"
}

variable "stickiness_enabled" {
  description = "Enable stickiness, default is disabled"
  default     = false
}

variable "stickiness_cookie_duration" {
  description = "The time period, in seconds, during which requests from a client should be routed to the same target. Default is 1 day (86400 seconds)."
  default     = 86400
}
