output "alb_name" {
  description = "Name of the ALB"
  value       = "${aws_lb.instance.name}"
}

output "alb_id" {
  description = "ID of the ALB"
  value       = "${aws_lb.instance.id}"
}

output "alb_dns_name" {
  description = "DNS name of the ALB"
  value       = "${aws_lb.instance.dns_name}"
}

output "alb_zone_id" {
  description = "Hosted route53 zone_id of the ALB (to be used in a Route 53 Alias record)"
  value       = "${aws_lb.instance.zone_id}"
}

output "security_group_id" {
  description = "Security Group ID of the ALB"
  value       = "${aws_security_group.alb_security_group.id}"
}

output "http_listener_arn" {
  description = "Default HTTP Listener arn"
  value       = "${aws_lb_listener.http_listener.*.arn}"
}

output "https_listener_arn" {
  description = "Default HTTPS Listener arn"
  value       = "${aws_lb_listener.https_listener.*.arn}"
}

output "http_target_group_arn" {
  description = "ARN of the HTTP Target Group"
  value       = "${aws_lb_target_group.http_target.*.arn}"
}

output "https_target_group_arn" {
  description = "ARN of the HTTPS Target Group"
  value       = "${aws_lb_target_group.https_target.*.arn}"
}
